/*eslint no-undef: "error"*/
/*eslint-env node*/

module.exports = function(grunt) {
  var shell          = require('shelljs');
  var bourbon_path   = shell.exec('bundle show bourbon', { async: false, silent: true }).stdout.replace('\n', '') + '/app/assets/stylesheets';
  var neat_path      = shell.exec('bundle show neat', { async: false, silent: true }).stdout.replace('\n', '') + '/app/assets/stylesheets';
  var support_path   = shell.exec('bundle show support-for', { async: false, silent: true }).stdout.replace('\n', '') + '/sass';
  var typey_path     = shell.exec('bundle show typey', { async: false, silent: true }).stdout.replace('\n', '') + '/stylesheets';
  var normalize_path = shell.exec('bundle show normalize-scss', { async: false, silent: true }).stdout.replace('\n', '') + '/sass';

  grunt.initConfig({
    php: {
      watch: {
        options: {
          hostname: '0.0.0.0',
          port: 3000,
          base: './',
          open: true
        }
      }
    },
    githooks: {
      all: {
        options: {
          hashbang: '#!/bin/sh',
          template: './node_modules/grunt-githooks/templates/shell.hb',
          startMarker: '## GRUNT-GITHOOKS START',
          endMarker: '## GRUNT-GITHOOKS END'
        },
        'pre-commit': 'scsslint eslint'
      }
    },
    sprite: {
      normal: {
        src: ['assets/img/sprite-src/*.{png,jpg}'],
        dest: 'assets/img/sprite.png',
        imgPath: '../img/sprite.png',
        destCss: 'assets/sass/support/_sprite.scss',
        padding: 5
      },
      retina: {
        src: ['assets/img/sprite-src/*.png'],
        retinaSrcFilter: ['assets/img/sprite-src/*@2x.{png,jpg}'],
        dest: 'assets/img/sprite.png',
        imgPath: '../img/sprite.png',
        retinaDest: 'assets/img/sprite@2x.png',
        destCss: 'assets/sass/support/_sprite.scss',
        retinaImgPath: '../img/sprite@2x.png',
        padding: 5
      }
    },
    scsslint: {
      allFiles: ['assets/sass/**/*.scss'],
      options: {
        bundleExec: true,
        config: '.scss-lint.yml',
        colorizeOutput: true
      }
    },
    eslint: {
      options: {
        configFile: '.eslintrc.json',
        quiet: true,
        maxWarnings: 20
      },
      target: ['Gruntfile.js', 'assets/js/*.js', 'assets/js/**/*.js']
    },
    sass: {
      dist: {
        options: {
          style: 'compressed',
          loadPath: [
            neat_path,
            bourbon_path,
            support_path,
            typey_path,
            normalize_path,
            'assets/sass'
          ],
          sourcemap: 'none',
          require: 'sass-globbing',
          noCache: true
        },
        files: {
          'assets/css/application.css':'assets/sass/application.scss'
        }
      },
      dev: {
        options: {
          style: 'expanded',
          loadPath: [
            neat_path,
            bourbon_path,
            support_path,
            typey_path,
            normalize_path,
            'assets/sass'
          ],
          sourcemap: 'none',
          require: 'sass-globbing',
          noCache: true
        },
        files: {
          'assets/css/application.css':'assets/sass/application.scss'
        }
      }
    },
    watch: {
      layout: {
        files: ['application/views/**/*.php', 'application/modules/**/views/**/*.php'],
        options: {
          spawn: false,
          livereload: true
        }
      },
      styles: {
        files: 'assets/sass/**/*.scss',
        tasks: ['sass:dev'],
        options: {
          spawn: false,
          livereload: true
        }
      },
      scripts: {
        files: 'assets/js/**/*.js',
        tasks: ['eslint'],
        options: {
          livereload: true
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-php');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-githooks');
  grunt.loadNpmTasks('grunt-scss-lint');
  grunt.loadNpmTasks('grunt-eslint');
  grunt.loadNpmTasks('grunt-spritesmith');

  grunt.registerTask('default', ['watch']);
  grunt.registerTask('server', ['php', 'watch']);
  grunt.registerTask('build', ['scsslint', 'sprite:retina', 'sass:dist']);
};
