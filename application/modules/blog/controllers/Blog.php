<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('blog_post');
		$this->load->helper('url');
		$this->template->set_regions(array('og_description', 'og_keywords', 'og_url', 'og_image'), FALSE);
		$this->template->write('body_class', 'blog');
	}

	public function index()
	{
		$data = array(
				'blog' => $this->blog_post->where('published', 1)->order_by('created', 'desc')->get()
			);

		$this->template->write('title', 'Blog - Fleteks');
		$this->template->write('og_description', '');
		$this->template->write('og_keywords', '');
		$this->template->write_view('content', 'index', $data);
		$this->template->render();
	}

	public function post($slug = '')
	{
		$query = $this->blog_post->where('slug', $slug)->where('published', 1)->get();

		if ($query->num_rows() < 1) {
			redirect('blog');
		}

		$post = $query->row();
		$this->template->write('title', $post->title.' - Fleteks');
		$this->template->write('og_description', $post->extract);
		$this->template->write('og_keywords', $post->keywords);
		$this->template->write('og_url', '/blog/post/'.$post->slug);
		$this->template->write('og_image', $post->image);
		$this->template->write_view('content', 'article', array('post' => $post));
		$this->template->render();
	}

	public function image($id)
	{
		if (! $this->blog_post->exists($id)) {
			redirect('blog');
		}

		$post = $this->blog_post->get($id)->row();

		if (empty($post->image)) {
			$config["source_image"] = './assets/img/social-image.jpg';
		} else {
			$config["source_image"] = '.'.$post->image;
		}

		$config["width"]  = 500;
		$config["height"] = 500;
		$config["dynamic_output"] = TRUE;

		$this->load->library('image_lib', $config);
		$this->image_lib->fit();
	}

}

/* End of file Blog.php */
/* Location: ./application/controllers/Blog.php */
