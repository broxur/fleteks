<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('blog_post');
	}

	public function index()
	{
		if ($this->input->post('del')) {
			$this->blog_post->delete($this->input->post('del'));
			$this->session->set_flashdata('msg_success', 'Las publicaciones han sido eliminadas de forma permanente.');
			redirect('admin/blog');
		}

		$this->load->library('pagination');
		$default     = array('search', 'offset');
		$param       = $this->uri->uri_to_assoc(3, $default);
		$num_results = 15;

		$param['search']     = ($this->input->post('search') != '') ? $this->input->post('search', TRUE):$param['search'];
		$data                = array();
		$data['msg_success'] = $this->session->flashdata('msg_success');
		$data['query']       = $this->blog_post->search( $param['search'], $param['offset'], $num_results );
		$data['search']      = $param['search'];
		$data['form_action'] = '/admin/blog';

		if (empty($param['search'])) {
			unset($param['search']);
			$config['uri_segment'] = 4;
		} else {
			$config['uri_segment'] = 6;
		}

		$param['offset']      = '';
		$config['total_rows'] = $this->blog_post->found_rows();
		$config['base_url']   = '/admin/blog/'.$this->uri->assoc_to_uri($param);
		$config['per_page']   = $num_results;

		$this->pagination->initialize($config);

		$data['pages']       = $this->pagination->create_links();
		$config['num_links'] = 1;

		$this->pagination->initialize($config);
		$data['pages_mobile'] = $this->pagination->create_links();

		$this->template->asset_js('crud.js');
		$this->template->write('title', 'Blog');
		$this->template->write_view('content', 'admin/list', $data);
		$this->template->render();
	}

	public function add()
	{
		$this->form_validation->set_rules('data[title]', 'título', 'required|trim');
		$this->form_validation->set_rules('data[extract]', 'extracto', 'required|trim');
		$this->form_validation->set_rules('data[body]', 'contenido', 'required|trim');
		$this->form_validation->set_rules('data[published]', 'publicado', 'required|trim');

		if ($this->form_validation->run()) {
			$this->blog_post->insert($this->input->post('data', TRUE));
			$this->session->set_flashdata('msg_success', 'La publicación ha sido agregada.');
			redirect('admin/blog');
		}

		$data = $this->blog_post->prepare_data($this->input->post('data'));
		$data['form_title'] = 'Nueva Publicación';
		$data['breadcrumb'] = 'Nueva';

		if(!isset($_POST['data'])){
			$data['published'] = 1;
		}

		$this->template->write('title', 'Agregar Publicación');
		$this->template->write_view('content', 'admin/form', $data);
		$this->template->add_css('assets/vendor/switchery/switchery.css');
		$this->template->add_js('assets/vendor/switchery/switchery.js');
		$this->template->add_js('assets/vendor/ckeditor/ckeditor.js');
		$this->template->render();
	}

	public function edit($id = '')
	{
		if (! $this->blog_post->exists($id)) {
			redirect('admin/blog');
		}

		$this->form_validation->set_rules('data[title]', 'título', 'required|trim');
		$this->form_validation->set_rules('data[extract]', 'extracto', 'required|trim');
		$this->form_validation->set_rules('data[body]', 'contenido', 'required|trim');
		$this->form_validation->set_rules('data[published]', 'publicado', 'required|trim');

		if ($this->form_validation->run()) {
			$_POST['data']['id'] = $id;
			$this->blog_post->update($this->input->post('data', TRUE), $id);
			$this->session->set_flashdata('msg_success', 'La publicación ha sido actualizada.');
			redirect('admin/blog');
		}

		$stored = $this->blog_post->get($id)->row_array();
		$data   = $this->blog_post->prepare_data($this->input->post('data'), $stored);
		$data['form_title'] = 'Editar Publicación';
		$data['breadcrumb'] = 'Editar';

		if(!isset($_POST['data'])){
			$data['active'] = 1;
		}

		$this->template->write('title', 'Editar Publicación');
		$this->template->write_view('content', 'admin/form', $data);
		$this->template->add_css('assets/vendor/switchery/switchery.css');
		$this->template->add_js('assets/vendor/switchery/switchery.js');
		$this->template->add_js('assets/vendor/ckeditor/ckeditor.js');
		$this->template->add_js('assets/js/crud.js');
		$this->template->render();
	}

	public function delete($id = '')
	{
		if (!empty($id)) {
			$this->blog_post->delete($id);
			$this->session->set_flashdata('msg_success', 'La publicación ha sido eliminada.');
		}

		redirect('admin/blog');
	}

	public function delete_image()
	{
		if ($this->input->is_ajax_request()) {

			if ($this->input->post('id')) {
				$this->blog_post->delete_image($this->input->post('id'));
				$output = array('status' => 'success');
			} else {
				$output = array('status' => 'error');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));
		} else {
			redirect('admin/blog');
		}
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */
