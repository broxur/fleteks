<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog_Post extends MY_Model {

	protected $_table      = 'blog_posts';
	protected $_id         = 'id';
	protected $field_names = array('title', 'image', 'created', 'published', 'extract', 'body', 'slug', 'keywords', 'type');
	protected $file_fields = array('image');
	protected $pre_insert  = array('datetime_created', 'create_slug', 'upload_image', 'unset_id');
	protected $pre_update  = array('create_slug', 'upload_image', 'unset_id');
	protected $grid_fields = 'id, title, created, published';
	protected $folder      = '/uploads/blog/';
	private   $max_img     = 1200;


	public function __construct()
	{
		parent::__construct();
	}

	private function generate_slug($str)
	{
		$this->load->helper('string');
		$a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ?!&';
		$b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr   ';
		$str = utf8_decode($str);
		$str = strtr($str, utf8_decode($a), $b);
		$str = strtolower(trim($str));
		$str = preg_replace("/[^a-z0-9-]/", "-", $str);
		$str = preg_replace("/-+/", "-", $str);

		return utf8_encode(reduce_multiples($str, '-', TRUE));
	}

	private function check_slug($string, $id = FALSE, $count = 0)
	{
		$slug = ($count > 0) ? $string . '-' . $count : $string;
		$this->db->where('slug', $slug);

		if ($id !== FALSE) {
			$this->db->where($this->_id.'!=', $id);
		}

		if ($this->db->count_all_results($this->_table) > 0) {
			return $this->check_slug($string, $id, ++$count);
		} else {
			return $slug;
		}
	}

	protected function datetime_created($data)
	{
		$data['created'] = date('Y-m-d H:i:s');
		return $data;
	}

	protected function create_slug($data)
	{
		$data['slug'] = $this->generate_slug($data['title']);

		if (isset($data[$this->_id])) {
			$data['slug'] = $this->check_slug($data['slug'], $data[$this->_id]);
		} else {
			$data['slug'] = $this->check_slug($data['slug'], FALSE);
		}

		return $data;
	}

	protected function unset_id($data)
	{
		if (isset($data['id'])) {
			unset($data['id']);
		}

		return $data;
	}

	protected function upload_image($data)
	{
		if (isset($_FILES['image']) && !empty($_FILES['image']['name'])) {
			$ext      = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
			$filename = $this->folder.$this->calculate_id().'.'.strtolower($ext);

			if (isset($data[$this->_id])) {
				$row = $this->get($data[$this->_id])->row();
				if ($row->image != '') {
					@unlink('.'.$row->image);
				}
			}

			if (move_uploaded_file($_FILES['image']['tmp_name'], '.'.$filename)) {
				$data['image'] = $filename;
				$size         = getimagesize('.'.$filename);
				chmod ('.'.$filename , 0755);

				if ($size[0] > $this->max_img || $size[1] > $this->max_img) {
					$config['source_image']   = '.'.$filename;
					$config['maintain_ratio'] = TRUE;
					$config['width']          = $this->max_img;
					$config['height']         = $this->max_img;
					$this->load->library('image_lib', $config);

					if (!$this->image_lib->resize()) {
						show_error($this->image_lib->display_errors('<p>', '</p>'));
					}
				}
			}
		}

		return $data;
	}

	public function delete_image($id)
	{
		if (!empty($id)) {
			if (!is_array($id)) {
				$id = array($id);
			}

			$query = $this->db->where_in($this->_id, $id)->get($this->_table);

			foreach ($query->result() as $row) {
				if (!empty($row->image) && file_exists('.'.$row->image)) {
					unlink('.'.$row->image);
				}
			}

			clearstatcache();
			$this->db->where_in($this->_id, $id)->update($this->_table, array('image' => ''));
		}

		return $id;
	}

}

/* End of file Blog_post.php */
/* Location: ./application/models/Blog_post.php */
