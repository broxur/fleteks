<h2 class="title-primary title-primary__main">Blog</h2>

<?php foreach ($blog->result() as $post): ?>
  <article class="blog__card">
    <?php if (!empty($post->image)): ?>
      <a href="/blog/post/<?php echo $post->slug ?>" class="blog__image" style="background-image:url('<? echo $post->image ?>');"></a>
    <?php endif ?>

    <a href="/blog/post/<?php echo $post->slug ?>" class="blog__card__title"><?php echo $post->title ?></a>
    <div class="blog__card__resume">
      <?php echo $post->extract; ?>
    </div>
    <div class="blog__card__footer">
      <a href="/blog/post/<?php echo $post->slug ?>" class="btn btn--red">Nota Completa</a>
    </div>
  </article>
<?php endforeach ?>
