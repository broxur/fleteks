
    <div class="row">
      <div class="col-md-12">
        <h1><?php echo $form_title ?></h1>

        <ol class="breadcrumb">
          <li><a href="/admin">Inicio</a></li>
          <li><a href="/admin/blog">Blog</a></li>
          <li class="active"><?php echo $breadcrumb ?></li>
        </ol>
      </div>
    </div>

    <?php if (validation_errors() != ''): ?>
      <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <h4>Error</h4>
        La información está incompleta o errónea.
      </div>
    <?php endif ?>

    <section class="panel">
      <div class="panel-body">
        <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">

          <?php $error = form_error('data[title]'); ?>
          <div class="form-group<?php echo ($error != '') ? ' has-error' : ''; ?>">
            <label for="title" class="col-lg-2 control-label">Título</label>
            <div class="col-lg-5">
              <input type="text" name="data[title]" id="title" class="form-control" value="<?php echo set_value('title', $title); ?>" placeholder="título de la publicación">
            </div>
            <?php echo $error; ?>
          </div>

          <?php $error = form_error('data[extract]'); ?>
          <div class="form-group<?php echo ($error != '') ? ' has-error' : ''; ?>">
            <label for="extract" class="col-lg-2 control-label">Extracto</label>
            <div class="col-lg-5">
              <textarea rows="5" name="data[extract]" id="extract" class="form-control" placeholder="texto breve introductorio para vista previa"><?php echo set_value('extract', $extract); ?></textarea>
            </div>
            <?php echo $error; ?>
          </div>

          <?php $error = form_error('data[body]'); ?>
          <div class="form-group<?php echo ($error != '') ? ' has-error' : ''; ?>">
            <label for="body" class="col-lg-2 control-label">Contenido</label>
            <div class="col-lg-8">
              <textarea name="data[body]" id="body" class="form-control txt-editor"><?php echo set_value('body', $body); ?></textarea>
            </div>
            <?php echo $error; ?>
          </div>


          <?php $error = form_error('data[published]'); ?>
          <div class="form-group<?php echo ($error != '') ? ' has-error' : ''; ?>">
            <label for="published" class="col-lg-2 control-label">Publicado</label>
            <div class="col-lg-5">
              <input type="hidden" name="data[published]" value="0">
              <input type="checkbox" name="data[published]" value="1" class="js-switch" <?php echo validate_checkbox($published, 1); ?>>
            </div>
            <?php echo $error; ?>
          </div>

          <?php $error = form_error('image'); ?>
          <div class="form-group<?php echo ($error != '') ? ' has-error' : ''; ?>">
            <label for="image" class="col-md-2 control-label">Imagen</label>
            <div class="col-md-5">

              <?php if (!empty($image)): ?>
              <div class="row single-image">
                <div class="col-xs-9 col-lg-10">
                  <img src="<?php echo $image ?>" class="img-rounded">
                </div>
                <div class="col-xs-3 col-lg-2">
                  <button id="delete-image" data-url="/admin/blog/delete-image" data-id="<?php echo $id ?>" class="btn btn-danger" type="button"><i class="fa fa-trash"></i></button>
                </div>
              </div>
              <?php endif ?>

              <input type="file" name="image" id="image" class="form-control" value="">
            </div>
            <?php echo $error; ?>
          </div>

          <?php $error = form_error('data[keywords]'); ?>
          <div class="form-group<?php echo ($error != '') ? ' has-error' : ''; ?>">
            <label for="keywords" class="col-lg-2 control-label">Palabras Clave</label>
            <div class="col-lg-5">
              <input type="text" name="data[keywords]" id="keywords" class="form-control" value="<?php echo set_value('keywords', $keywords); ?>" placeholder="de 5 a 10 separadas por comas">
            </div>
            <?php echo $error; ?>
          </div>

          <?php $error = form_error('data[type]'); ?>
          <div class="form-group<?php echo ($error != '') ? ' has-error' : ''; ?>">
            <label for="type" class="col-lg-2 control-label">Tipo</label>
            <div class="col-lg-5">
              <select name="data[type]" id="type" class="form-control">
                <option value="0" <?php echo validate_select(0, $type) ?>>Nota</option>
                <option value="1" <?php echo validate_select(1, $type) ?>>Video</option>
              </select>
            </div>
            <?php echo $error; ?>
          </div>

          <div class="form-group">
            <div class="col-lg-3 col-sm-offset-2 btn-crud">
              <button type="submit" class="btn btn-success">Guardar</button>
              <a href="/admin/blog" class="btn btn-default">Regresar</a>
            </div>
          </div>

        </form>
      </div>
    </section>
