<h2 class="title-primary title-primary__main"><?php echo $post->title ?></h2>

<?php if (!empty($post->image)): ?>
  <div href="/blog/post/<?php echo $post->slug ?>" class="blog__image" style="background-image:url('<? echo $post->image ?>');"></div>
<?php endif ?>

<p class="text-right"><strong>Publicado:</strong> <?php echo $post->created ?></p>

<?php echo $post->body; ?>

<p class="text-center">
  <a href="/blog" class="btn btn--red">Regresar</a>
</p>
