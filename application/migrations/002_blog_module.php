<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Blog_Module extends CI_Migration {

	public function up()
	{
		$attributes = array('ENGINE' => 'MyISAM', 'DEFAULT CHARSET' => 'utf8');
		$fields = array(
				"`id` int(9) NOT NULL AUTO_INCREMENT PRIMARY KEY",
				"`title` varchar(250) NOT NULL DEFAULT ''",
				"`image` varchar(250) NOT NULL DEFAULT ''",
				"`created` datetime NOT NULL",
				"`published` int(1) NOT NULL DEFAULT 0",
				"`extract` text NOT NULL DEFAULT ''",
				"`body` mediumtext NOT NULL DEFAULT ''",
				"`slug` varchar(150) NOT NULL DEFAULT ''",
				"`keywords` varchar(150) NOT NULL DEFAULT ''",
				"`type` int(1) NOT NULL DEFAULT 0",
			);
		$this->dbforge->add_field($fields);
		$this->dbforge->create_table('blog_posts', TRUE, $attributes);
	}

	public function down()
	{
		$this->dbforge->drop_table('blog_posts');
	}

}
