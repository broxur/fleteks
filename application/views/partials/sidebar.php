<div class="side-box side-box--toll-free">
  <div class="side-box__text">
    <span class="icon icon-phone-red"></span>
    Llama sin costo: <br>
    <span class="side-box__bold-red">01 800 55 71 921</span>
  </div>
</div>
<div class="side-box side-box--quotation">
  <div class="side-box__text side-box__text--quotation">
    <span class="icon icon-truck-yellow"></span>
    Solicita una cotización aquí
  </div>
</div>
<form action="" class="side-form">
  <div class="side-form__group">
    <label for="name" class="side-form__label">Nombre(s):</label>
    <input type="text" id="name" name="name" class="side-form__control">
  </div>
  <div class="side-form__group">
    <label for="last_name" class="side-form__label">Apellido:</label>
    <input type="text" id="last_name" name="last_name" class="side-form__control">
  </div>
  <div class="side-form__group">
    <label for="phone" class="side-form__label">Teléfono:</label>
    <input type="text" id="phone" name="phone" class="side-form__control">
  </div>
  <div class="side-form__group">
    <label for="mail" class="side-form__label">Correo:</label>
    <input type="text" id="mail" name="mail" class="side-form__control">
  </div>
  <div class="side-form__group">
    <label for="origin" class="side-form__label">Origen:</label>
    <input type="text" id="origin" name="origin" class="side-form__control">
  </div>
  <div class="side-form__group">
    <label for="destination" class="side-form__label">Destino:</label>
    <input type="text" id="destination" name="destination" class="side-form__control">
  </div>
  <div class="side-form__group">
    <label for="comments" class="side-form__label">Comentarios:</label>
    <textarea name="comments" id="comments" rows="5" class="side-form__control side-form__textarea"></textarea>
  </div>
  <div class="side-form__group">
    <button id="quotation-submit" type="button" class="btn btn--green side-form--submit">Recibir Descuento</button>
    <p class="side-form__disclaimer">Respetamos tu privacidad y no compartiremos tu información con nadie. <a href="#">Privacidad y Cookies</a></p>
  </div>
</form>
