<div class="side-box side-box--toll-free">
  <div class="side-box__text">
    <span class="side-box__bold-red">¡Importante <br> Mencionar!</span>
  </div>
</div>
<form action="" class="side-form">
  <div class="side-form__group">
    <p>Número de muebles, cajas, etc.</p>
    <p>Fecha de Envío</p>
    <p>¿Hay escaleras?</p>
    <p>¿Exclusiva o compartida?</p>
    <p>Origen y destino</p>
    <p>¿Enviará su auto?</p>
    <p>¿Cuenta con empaques y cajas?</p>
  </div>
</form>
