<nav id="topnav" class="topnav">
  <div class="container">
    <div class="topnav__header">
      <span class="icon icon-logo-mobile">&nbsp;</span>
      <a href="#" class="navicon"><span>&#9776;</span></a>
    </div>
    <ul class="topnav__list">
      <li><a class="topnav__link" href="/">Inicio</a></li>
      <li><a class="topnav__link" href="/mudanzas">Mudanzas</a></li>
      <li><a class="topnav__link" href="/servicios">Servicios</a></li>
      <li><a class="topnav__link" href="/vehiculos">Vehículos</a></li>
      <li><a class="topnav__link" href="/preguntas-frecuentes">Preguntas</a></li>
      <li><a class="topnav__link" href="/blog">Blog</a></li>
      <li><a class="topnav__link" href="/contacto">Contacto</a></li>
    </ul>
  </div>
</nav>
