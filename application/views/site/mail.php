<?php echo $nombre ?> <?php echo $apellidos ?> desea ser contactado para recibir más información sobre los servicios de Fleteks y ha dejado los siguientes datos de contacto:

Correo: <?php echo $correo ?>

<?php if (isset($telefono) && !empty($telefono)): ?>
Teléfono: <?php echo $telefono ?>

<?php endif ?>
<?php
if (isset($mensaje) && !empty($mensaje)){
  echo $mensaje;
}
?>
