<h2 class="title-primary title-primary__main">Mudanzas</h2>

<section class="section-hero">
  <img class="section-hero--image" src="/assets/img/familia.jpg" alt="" />
</section>

<section class="moves__column-container">
  <div class="column-split-2 moves__column">
    <div class="moves__column__header">
      Mudanza Exclusiva
    </div>

    <div class="moves__column__content">
      <ol class="moves__advantages">
        <li>Recolección y entrega a domicilio.</li>
        <li>Maniobras en general de carga y descarga.</li>
        <li>Servicio directo sin escala.</li>
        <li>Seguro de mudanza (opcional)</li>
        <li>Grúas para vehículo (opcional).</li>
        <li>Entrega de 36 a 48 horas.</li>
        <li>Protección básica, inventarios, etc.</li>
        <li>GPS rastreo satelital.</li>
      </ol>
    </div>
  </div>
  <div class="column-split-2 moves__column">
    <div class="moves__column__header">
      Mudanza Compartida
    </div>

    <div class="moves__column__content">
      <ol class="moves__advantages">
        <li>Recolección y entrega a domicilio.</li>
        <li>Maniobraws de carga y descarga en general.</li>
        <li>Tiempo de entrega de 3 a 5 días hábiles.</li>
        <li>Grúas para vehículo (opcional).</li>
        <li>Seguro de Mudanza (opcional).</li>
      </ol>
    </div>
  </div>
</section>
