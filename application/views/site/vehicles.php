<h2 class="title-primary title-primary__main">Vehículos</h2>

<section class="section-hero">
  <img class="section-hero__image" src="/assets/img/camiones.jpg" alt="" />
</section>

<div class="vehicles__item clearfix">
  <div class="vehicles__item__image"></div>
  <div class="vehicles__item__content">
    <h3 class="vehicles__item__title">Camiones medianos de 3 toneladas</h3>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
    Doloremque tempore sit dolores rem optio repellat ipsa
    voluptates fuga veritatis similique error laborum sed
    blanditiis, fugiat veniam expedita aut voluptate, a.
  </div>
</div>

<div class="vehicles__item clearfix">
  <div class="vehicles__item__image"></div>
  <div class="vehicles__item__content">
    <h3 class="vehicles__item__title">Planas 3.5/6/15 toneladas</h3>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
    Doloremque tempore sit dolores rem optio repellat ipsa
    voluptates fuga veritatis similique error laborum sed
    blanditiis, fugiat veniam expedita aut voluptate, a.
  </div>
</div>

<div class="vehicles__item clearfix">
  <div class="vehicles__item__image"></div>
  <div class="vehicles__item__content">
    <h3 class="vehicles__item__title">Camiones con rampa</h3>
    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
    Doloremque tempore sit dolores rem optio repellat ipsa
    voluptates fuga veritatis similique error laborum sed
    blanditiis, fugiat veniam expedita aut voluptate, a.
  </div>
</div>
