<h2 class="title-primary title-primary__main">Contacto</h2>
<p class="contact-us__text">¿Necesitas una cotización? ¿Aún tienes dudas? <br>
  Escríbenos y resolveremos todas tus dudas.</p>

<form action="" class="contact-us__form">
  <div class="contact-us__group">
    <label for="name" class="contact-us__label">Nombre(s):</label>
    <input type="text" id="name" name="name" class="contact-us__control">
  </div>
  <div class="contact-us__group">
    <label for="last_name" class="contact-us__label">Apellido:</label>
    <input type="text" id="last_name" name="last_name" class="contact-us__control">
  </div>
  <div class="contact-us__group">
    <label for="phone" class="contact-us__label">Teléfono:</label>
    <input type="text" id="phone" name="phone" class="contact-us__control">
  </div>
  <div class="contact-us__group">
    <label for="mail" class="contact-us__label">Correo:</label>
    <input type="text" id="mail" name="mail" class="contact-us__control">
  </div>
  <div class="contact-us__group">
    <label for="origin" class="contact-us__label">Origen:</label>
    <input type="text" id="origin" name="origin" class="contact-us__control">
  </div>
  <div class="contact-us__group">
    <label for="destination" class="contact-us__label">Destino:</label>
    <input type="text" id="destination" name="destination" class="contact-us__control">
  </div>
  <div class="contact-us__group">
    <label for="comments" class="contact-us__label">Comentarios:</label>
    <textarea name="comments" id="comments" rows="5" class="contact-us__control contact-us__textarea"></textarea>
  </div>
  <div class="contact-us__group text-center">
    <button id="quotation-submit" type="button" class="btn btn--green contact-us__submit">Enviar</button>
  </div>
</form>
