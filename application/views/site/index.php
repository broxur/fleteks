<section id="slider" class="slider">
  <div class="slider__content" data-slide="0">Las mudanzas <br> mas seguras al <br> mejor precio</div>
  <div class="slider__content" data-slide="1">Las mudanzas <br> mas seguras al <br> mejor precio</div>
  <div class="slider__content" data-slide="2">Las mudanzas <br> mas seguras al <br> mejor precio</div>
  <div class="slider__content" data-slide="3">Las mudanzas <br> mas seguras al <br> mejor precio</div>
  <div class="slider__content" data-slide="4">Las mudanzas <br> mas seguras al <br> mejor precio</div>
</section>
<div class="slider__pagers">
  <i id="pager-0" class="slider__pagers__item" data-slider="0"></i>
  <i id="pager-1" class="slider__pagers__item" data-slider="1"></i>
  <i id="pager-2" class="slider__pagers__item" data-slider="2"></i>
  <i id="pager-3" class="slider__pagers__item" data-slider="3"></i>
  <i id="pager-4" class="slider__pagers__item" data-slider="4"></i>
</div>
<section class="home__services">
  <h2 class="title-secondary">Nuestros Servicios</h2>
  <div>
    <div class="column-split-2 preview-card">
      <h3 class="preview-card__title">Mudanzas</h3>

      <p class="preview-card__text">Nuestro servicio abarca de manera integral el concepto
      "mudanza", ya que realizamos un levantamiento total de sus
      perenencias, les protegemos, inventariamos, depositamos...</p>

      <div class="preview-card__button">
        <a href="/servicios" class="btn btn--red">Leer más</a>
      </div>
    </div>

    <div class="column-split-2 preview-card">
      <h3 class="preview-card__title">Transporte de carga en general</h3>

      <p class="preview-card__text">El transporte de carga en general en Fleteks brinda
      servicio de envíos empresariales y particulares en unidades
      de gran tamaño y capacidad de carga, ideales para...</p>

      <div class="preview-card__button">
        <a href="/servicios" class="btn btn--red">Leer más</a>
      </div>
    </div>
  </div>
</section>

<section class="home__blog">
  <h2 class="title-secondary">Blog</h2>

  <div>
    <?php foreach ($articles->result() as $post): ?>

    <div class="column-split-2 preview-card">
      <div class="preview-card__image" style="background-image: url(/blog/image/<?php echo $post->id ?>);"></div>

      <h3 class="preview-card__title"><?php echo $post->title ?></h3>

      <p class="preview-card__text">
        <?php echo $post->extract ?>
      </p>

      <div class="preview-card__button">
        <a href="/blog/post/<?php echo $post->slug ?>" class="btn btn--red">Leer más</a>
      </div>
    </div>

    <?php endforeach ?>
  </div>
</section>
<script type="text/javascript">
  var slider_images = [
        { src: "/assets/img/dummies/01.jpg" },
        { src: "/assets/img/dummies/02.jpg" },
        { src: "/assets/img/dummies/03.jpg" },
        { src: "/assets/img/dummies/04.jpg" },
        { src: "/assets/img/dummies/05.jpg" }
      ]
</script>
