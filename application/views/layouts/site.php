<!DOCTYPE html>
<html class="no-js" lang="es-mx">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Fleteks | <?php echo $title ?></title>
  <meta name="keywords" content="">
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="/assets/css/application.css">
  <?= $_styles ?>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script src="/assets/vendor/modernizr.js"></script>
</head>
<body<?= (!empty($body_class)) ? ' class="'.$body_class.'"':'' ?>>
  <div class="wrapper">
    <div class="header">
      <div class="container">
        <div class="header__logo">
          <h1 class="icon icon-logo">Fleteks</h1>
        </div>
        <div class="header__slogan">
          Soluciones Integrales en Transporte
        </div>
        <div class="header__phone">
          <span class="header__red-rounded"><span class="icon icon-phone-white"></span></span> 01 800 55 71 921
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <?= $topnav ?>
    <div class="container content-top-margin">
      <div class="main-content">
        <?= $content ?>
      </div>
      <aside class="sidebar">
        <?= $sidebar ?>
        <div class="side-box side-box--payments">
          <div class="side-box__text side-box__text--payments">
            <span class="icon icon-payments"></span> <br>
            Aceptamos todas las tarjetas de crédito.
        </div>
      </aside>
    </div>
  </div>
  <footer class="footer">
    <div class="container">
      Fleteks &copy; 2016
    </div>
  </footer>
  <script src="/assets/vendor/jquery-3.1.0.min.js"></script>
  <script src="/assets/vendor/sweetalert/sweetalert.min.js"></script>
  <script src="/assets/js/components/responsive-nav.js"></script>
  <script src="/assets/js/components/mail-quotation.js"></script>
  <?= $_scripts ?>
</body>
</html>
