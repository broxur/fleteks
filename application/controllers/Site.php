<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->model('blog/blog_post');
		$data = array(
			'articles' => $this->blog_post->where('published', 1)->limit(2)->order_by('created', 'DESC')->get()
		);

		$this->template->write('body_class', 'home');
		$this->template->write('title', 'Inicio');
		$this->template->write_view('content', 'site/index', $data);
		$this->template->add_css('assets/vendor/vegas/vegas.min.css');
		$this->template->add_js('assets/vendor/vegas/vegas.min.js');
		$this->template->add_js('assets/js/components/slider.js');
		$this->template->render();
	}

	public function about()
	{
		$this->template->write('body_class', 'about-us');
		$this->template->write('title', 'Nosotros');
		$this->template->write_view('content', 'site/about');
		$this->template->render();
	}

	public function services()
	{
		$this->template->write('body_class', 'services');
		$this->template->write('title', 'Servicios');
		$this->template->write_view('content', 'site/services');
		$this->template->render();
	}

	public function moves()
	{
		$this->template->write('body_class', 'moves');
		$this->template->write('title', 'Mudanzas');
		$this->template->write_view('content', 'site/moves');
		$this->template->render();
	}

	public function vehicles()
	{
		$this->template->write('body_class', 'vehicles');
		$this->template->write('title', 'Vehículos');
		$this->template->write_view('content', 'site/vehicles');
		$this->template->render();
	}

	public function faqs()
	{
		$this->template->write('body_class', 'faqs');
		$this->template->write('title', 'Preguntas Frecuentes');
		$this->template->write_view('content', 'site/faqs');
		$this->template->render();
	}

	public function contact_us()
	{
		$this->template->write('body_class', 'contact-us');
		$this->template->write('title', 'Contacto');
		$this->template->write_view('sidebar', 'partials/side-contact', NULL, TRUE);
		$this->template->write_view('content', 'site/contact_us');
		$this->template->render();
	}

	public function mail()
	{
		if ($this->input->is_ajax_request()) {
			$this->load->library(array('email', 'form_validation'));
			$this->form_validation->set_rules('nombre', 'nombre', 'required|trim');
			$this->form_validation->set_rules('apellidos', 'apellidos', 'required|trim');
			$this->form_validation->set_rules('correo', 'correo', 'required|valid_email|trim');

			if ($this->form_validation->run()) {
				$data = array(
						'nombre'    => $this->input->post('nombre', TRUE),
						'apellidos' => $this->input->post('apellidos', TRUE),
						'telefono'  => $this->input->post('telefono', TRUE),
						'mensaje'   => $this->input->post('mensaje', TRUE),
						'correo'    => $this->input->post('correo', TRUE)
					);

				$body = $this->load->view('site/mail', $data, TRUE);
				$this->email->to('chicharomagico@gmail.com');
				$this->email->to('ventas@fleteks.com');
				$this->email->from('contacto@fleteks.com.mx', 'Fleteks');
				$this->email->subject('Contacto desde Sitio');
				$this->email->message($body);

				if (!$this->email->send()) {
					$output = array('status' => 'error', 'msg' => $this->email->print_debugger(array('headers')));
				} else {
					$output = array('status' => 'success', 'msg' => 'Correo enviado');
				}
			} else {
				$output = array('status' => 'error', 'msg' => 'Error en la validación de datos.');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($output));

		} else {
			show_404();
		}
	}

}

/* End of file Site.php */
/* Location: ./application/controllers/Site.php */
