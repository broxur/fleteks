<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * composer autoload is required in order to
 * initialize and use Whoops object.
 */

require_once('./vendor/autoload.php');
use Whoops\Handler\JsonResponseHandler;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Handler\PlainTextHandler;
use Whoops\Run;

function load_whoops()
{
	$whoops = new Run();
	$whoops->pushHandler(new PrettyPageHandler());

	/* checks if is an ajax call */
	if (\Whoops\Util\Misc::isAjaxRequest()) {
		$jsonHandler = new JsonResponseHandler();
		$jsonHandler->addTraceToOutput(true);
		$whoops->pushHandler($jsonHandler);
	}

	/* checks if is a command line call */
	if (\Whoops\Util\Misc::isCommandLine()){
		$whoops->pushHandler(new PlainTextHandler());
	}

	$whoops->register();
}
