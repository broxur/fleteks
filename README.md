# Fleteks

## Requerimientos para desarrollo

- Ruby 2.3.1
- Node 4.7.7
- Grunt 1.0

Se aconseja hacer uso de rvm o rbenv para el caso de ruby y nvm para el caso de node.

## Instalación

Se hace uso de gemas sass, neat, bourbon y scss-lint. Adicionalmente se usa typey
para el manejo de conversión de unidades de pixels a rems y support-for para media queries.
Para la instalación de las gemas es necesario haber instalado ruby mediante rbenv o rvm
y en consola:

```
$ rvm use 2.3.1
$ gem install bundler
$ bundle install
```

Para el caso de Grunt, se aconseja usar haber instalado el paquete de forma global:

```
$ npm install -g grunt-cli
```

y a continuación se instalan los paquetes necesarios para el proyecto

```
$ npm install
```

Configuración del hook pre-commit

```
$ grunt githooks
```
