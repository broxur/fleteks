/*global swal*/

function validate_email(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

$(function() {
  /**
   * function for contact form
   * @return void
   */
  var mail_quotation = function() {
    var error = '';

    if ($('#name').val() == '') {
      error += 'El campo nombre es requerido.\n';
    }

    if ($('#last_name').val() == '') {
      error += 'El campo apellidos es requerido.\n';
    }

    if ($('#phone').val() == '') {
      error += 'Proporcione su número telefónico.\n';
    }

    if (!validate_email($('#mail').val())) {
      error += 'Debe de proporcionar una dirección de correo.\n';
    }

    if (error == '') {
      $.ajax({
        url: '/site/mail',
        type: 'POST',
        dataType: 'json',
        data: {
          nombre: $('#name').val(),
          apellidos: $('#last_name').val(),
          correo: $('#mail').val(),
          telefono: $('#phone').val(),
          mensaje: $('#message').val()
        },
      })
      .done(function(data) {
        if (data.status === 'error') {
          swal('Ups', 'Hubo una falla en el envío.', 'error');
        } else {
          swal('¡Enviado!', 'Hemos recibido su mensaje y pronto nos pondremos en contacto.', 'success');
          $('#name, #last_name, #phone, #mail, #message').val('');
        }
      })
      .fail(function() {
        swal('Ups', 'Hubo una falla en el envío');
      });
    } else {
      swal('Información incorrecta', error, 'warning');
    }
  };

  $('#quotation-submit').click(function() {
    mail_quotation();
  });
});
