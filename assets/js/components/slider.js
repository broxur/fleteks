/*global slider_images*/

function next_content(i) {
  if ( $('div.slider__content[data-slide=' + i + ']').length == 0 ) {
    return false;
  } else {
    $('div.slider__content').hide();
    $('div.slider__content[data-slide=' + i + ']').fadeIn('slow');
  }
}

$(function() {
  if (typeof slider_images != 'undefined') {
    $('#slider').vegas({
      slides: slider_images,
      walk:function(step) {
        next_content(step);
        $('.slider__pagers i').removeClass('active');
        $('#pager-' + step).addClass('active');
      }
    });

    $('.slider__pagers i').click(function() {
      $('#slider').vegas('jump', $(this).data('slider'));
    });
  }
});
