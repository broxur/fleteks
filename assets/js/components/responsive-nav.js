$(function() {
  /**
   * hide or show menu nav links
   * based on viewport width
   */
  var verify_menu = function() {
    $('.navicon').removeClass('active');
    if (Modernizr.mq('(min-width: 695px)')) {
      $('.topnav__list').show();
    } else {
      $('.topnav__list').hide();
    }
  };

  /**
   * bind functions for hamburguer menu
   * for responsive navbar
   */
  $('a.navicon').click(function() {
    $(this).toggleClass('active');
    $('ul.topnav__list').slideToggle(400);
    return false;
  });

  /**
   * bind function to window events for responsive
   * styles and effects
   */
  $(window).on('resize', function() {
    verify_menu();
  });
});
